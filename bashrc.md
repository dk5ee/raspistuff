useful stuff for bashrc

## Sound und Video

play a video with omxplayer with offsets. for example on attached CRT monitor
```
playvid() {
	omxplayer -b --win 48,64,720,512 "$1"
}
```

display an image in frambuffer
```
alias image='fim -H -N -q '
```

clear framebuffer from console output
```
alias blank='dd if=/dev/zero of=/dev/fb0'
```

play an online stream, here fm4 austrian radio
```
alias fm4='mplayer http://mp3stream1.apasf.apa.at:8000'
```

