#!/bin/bash

#
# fetches rain radar gif from website and displays it on framebuf device
#

mkdir -p /tmp/rain
cd /tmp/rain
rm -rf *.gif
rm -rf fram*.*
wget "https://www.niederschlagsradar.de/image.ashx?type=regioloop&regio=nrb&j=-3&m=&d=&mi=&uhr=&bliksem=0&voor=&srt=loop1stunde" -O rain.gif

convert -coalesce rain.gif frame%05d.pgm
sudo fbi -noverbose -a -T 1 -t 1 frame*
read -p "Press any key to continue... " -n1 -s
sudo killall fbi
rm -rf *.gif
rm -rf fram*.*
