
# Setup Raspberry Pi

## Debian

1. Get Image from https://www.raspberrypi.org/downloads/
2. unzip image and  write to sd-card
3. activate ssh - empty file "ssh" in boot partition
4. optional: add wifi keys
5. stick sd card into raspberry and power up
6. for safety: regenerate keys
7. edit /etc/hostname for new name

### write to sd-card

Linux:

```
dd if=20yy-mm-dd-raspbian-buster-lite of=/dev/sdx bs=1M
```

### add wifi

in boot partion add empty file:

ssh

in boot partion add file:

wpa_supplicant.conf
```
country=DE
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
network={
    ssid="WIFI_NAME"
    psk="WIFI_PASSWORD"
    key_mgmt=WPA-PSK
    id_str="network1"
    priority=2
}
network={
    ssid="SECOND_WIFI"
    psk="WIFI2_PASSWORD"
    key_mgmt=WPA-PSK
    id_str="network2"
    priority=1
}
network={
    ssid="FREIFUNK_WIFI"
      key_mgmt=NONE
      id_str="freifunk"
      priority=0
}
```
higher priority means: use this network

### find your pi in network
```
nmap -sP 192.168.1.0/24
```

### ssh keys

regenerate keys:
```
sudo rm /etc/ssh/ssh_host*
sudo ssh-keygen -A
```

passwordless ssh access - login as user pi
```
#delete old keys if exists - probably DANGEROUS
rm -f ~/.ssh/id*
#generate new keys, on raspi and on every linuxuser which should have access
ssh-keygen
#..press enter multiple times
#longer key with comment
ssh-keygen -b 4048 -t rsa -C "raspberypikey"
#copy key to raspberry pi authorized keys
cat ~/.ssh/id_rsa.pub | ssh <pi>@<IP> 'mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys'
```

